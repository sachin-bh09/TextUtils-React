import React, { useState } from 'react'

export default function Textform(props) {
    document.title = "TextUtils - Home"
    const [text, setText] = useState("")

    function handleUpClick() {
        let newText = text.toUpperCase();
        setText(newText);
        props.showAlert("Converted to Uppercase", "success")
    }

    function handleLowClick() {
        let newText = text.toLowerCase();
        setText(newText);
        props.showAlert("Converted to Lowercase", "success")
    }

    function handleClearClick() {
        let newText = "";
        setText(newText);
    }

    const handleOnChange = (event) => {
        setText(event.target.value)
    }

    let textLength = text.length

    const preview = () => {
        document.getElementById("preview").innerHTML = textLength > 0 ? text : "Please Enter text above to preview"
        setTimeout(() => {
            document.getElementById("preview").innerHTML = ""
            handleClearClick()
        }, 2000);
    }

    let numberOfWords = text.split(" ").length
    return (
        <>
            <div className={`container bg-${props.mode === 'light' ? 'white' : '#042743'} text-${props.mode === 'light' ? 'dark' : 'light'}`}>
                <h1>{props.heading}</h1>
                <div className="mb-3">
                    <textarea className="form-control" style={{backgroundColor: props.mode === 'light' ? 'white' : 'grey', color: props.mode === 'light' ? 'black' : 'white', borderColor: 'grey'}} 
                    value={text} placeholder='Enter text here' onChange={handleOnChange} id="myBox" rows="8"></textarea>
                </div>
                <button className="btn btn-primary mx-1" onClick={handleUpClick}>Convert to Uppercase</button>
                <button className="btn btn-primary mx-1" onClick={handleLowClick}>Convert to Lowercase</button>
                <button className="btn btn-primary mx-1" onClick={handleClearClick}>Clear text</button>
            </div>
            <div className={`container my-3 bg-${props.mode === 'light' ? 'white' : '#042743'} text-${props.mode === 'light' ? 'dark' : 'light'}`}>
                <h1>Text Summary</h1>
                <p>{numberOfWords} Words and {textLength} Characters</p>
                <p>{0.008 * numberOfWords} Minutes for reading</p>
            </div>
            <button className="btn btn-primary" onClick={preview}>Preview</button>
            <div className="container">
                <p id="preview" style={{color: props.mode === 'light' ? 'black' : 'white'}}></p>
            </div>
        </>
    )
}
